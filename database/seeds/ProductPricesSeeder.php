<?php

use App\ProductPrices;
use Illuminate\Database\Seeder;

class ProductPricesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $arrayValues = ['daily','weekly','monthly','yearly'];
        for ($i = 1; $i <= 50; $i++){
            for ($j = 0; $j < count($arrayValues) - 1; $j++) {
                ProductPrices::create([
                    'product_id' => $i,
                    'product_price' => rand(50000,3000000),
                    'product_price_type' => $arrayValues[$j]
                ]);
            }
        }
    }
}
