<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserTableSeeder::class);
        $this->call(ProductTableSeeder::class);
        $this->call(ProductPricesSeeder::class);
        $this->call(AddressTableSeeder::class);
        $this->call(DeliveryTableSeeder::class);
        $this->call(ProductImageSeeder::class);
    }
}
