<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// Authentication
Route::group(['prefix' => 'auth'], function() {
    Route::post('login', 'AuthController@login');
    Route::post('register', 'RegisterController@register');

    Route::group(['middleware' => ['jwt.auth']], function() {
        Route::get('logout', 'AuthController@logout');
        Route::get('me', 'AuthController@me');
    });
});

//Read
Route::resource('products','ProductController');
Route::get('product-mostbought','ProductController@MostBought');
Route::get('product-NewlyCreated','ProductController@NewlyCreated');
Route::resource('products/{id}/prices','ProductPricesController');

Route::resource('product/{id}/images', 'ProductImageController');

Route::group(['prefix' => 'lease', 'middleware' => ['jwt.auth']], function() {
    Route::post('checkout', 'LeaseTransactionController@checkout');
    Route::post('pay', 'LeaseTransactionController@pay');
    Route::post('delivery', 'LeaseTransactionController@leaseDelivery');

    Route::get('transactions/{id}', 'LeaseTransactionController@showLease');
    Route::get('transactions-consumer', 'LeaseTransactionController@showAllTransaction');
    Route::get('transactions-provider','LeaseTransactionController@showAllConsumerTrans');
    Route::get('transactions-status','LeaseTransactionController@showAllBasedOnStatus');
});

Route::group(['prefix'=> 'leasereturn','middleware'=>['jwt.auth']],function(){
    Route::post('SendReturn','LeaseReturnController@sendReturn');
    Route::post('Arrived','LeaseReturnController@arrive');

    Route::get('LeaseReturn-Provider','LeaseReturnController@showProviderReturn');
    Route::get('LeaseReturn-Consumer','LeaseReturnController@showUserReturn');
});

Route::group(['middleware' => ['jwt.auth']], function() {
    Route::resource('user/address', 'AddressController');
    Route::resource('deliveries','DeliveryController');
    Route::resource('products', 'ProductController', ['only' => ['store', 'update', 'destroy']]);
});
