<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LeaseTransactionPenalty extends Model
{
    protected $table = 'lease_transaction_penalties';
    protected $fillable = ['lease_transaction_id','lease_penalty_fee','product_price_type'];
    
    public function leaseTransaction(){
        return $this->belongsTo('App\LeaseTransaction','lease_transaction_id');
    }

}
