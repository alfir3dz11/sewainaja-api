<?php

namespace App\Http\Requests;

use App\Http\Requests\ApiRequest;
use Illuminate\Foundation\Http\FormRequest;

class DeliveryUpdateRequest extends FormRequest{

    use ApiRequest;
    public function rules()
    {
        return [
            'delivery_date' => 'requierd',
            'delivery_receipt' => 'required'
        ];
    }
}
