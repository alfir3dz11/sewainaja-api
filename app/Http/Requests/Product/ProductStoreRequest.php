<?php

namespace App\Http\Requests\Product;

use App\Http\Requests\ApiRequest;
use Illuminate\Foundation\Http\FormRequest;

class ProductStoreRequest extends FormRequest
{
    use ApiRequest;

    public function rules() {
        return [
            'name' => 'required',
            'user_id' => 'required|exists:users,id',
            'stock' => 'required|integer',
            'description' => 'required',
            'penalty_fee' => 'required|integer'
        ];
    }
}
