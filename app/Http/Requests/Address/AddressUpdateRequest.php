<?php

namespace App\Http\Requests\Address;

use App\Http\Requests\ApiRequest;
use Illuminate\Foundation\Http\FormRequest;

class AddressUpdateRequest extends FormRequest
{

    use ApiRequest;

    public function rules()
    {
        return [
            'province' => 'required',
            'city' => 'required',
            'address' => 'required',
            'zipcode' => 'required',
        ];
    }
}
