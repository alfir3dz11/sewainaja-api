<?php

namespace App\Http\Controllers;

use App\Delivery;
use App\Http\Requests\LeaseTransaction\LeaseTransactionStoreRequest;
use App\Http\Requests\LeaseTransaction\LeaseTransactionStatusRequest;
use App\LeaseTransaction;
use App\LeaseTransactionDetail;
use App\Product;
use App\ProductPrices;
use App\Services\Response;
use Illuminate\Http\Request;
use Stripe\Charge;
use Stripe\Stripe;

class LeaseTransactionController extends Controller
{
    private $leaseTransaction;
    private $leaseTransactionDetail;
    private $product;
    private $productPrice;
    private $delivery;

    //This function constructs the order of the billing info
    public function __construct(
        LeaseTransaction $leaseTransaction,
        LeaseTransactionDetail $leaseTransactionDetail,
        Product $product,
        ProductPrices $productPrice,
        Delivery $delivery
    ) {
        $this->leaseTransaction = $leaseTransaction;
        $this->leaseTransactionDetail = $leaseTransactionDetail;
        $this->product = $product;
        $this->productPrice = $productPrice;
        $this->delivery = $delivery;
    }
    //This function checks out the item payed after the user clicks the following item. After clicking the item, it checks how many quantity they will pay. The product leaseTime determines how much time they will rent the item before it runs out to return. After clicking the item, it shows the data of the item mentioned.
    public function checkout(LeaseTransactionStoreRequest $request) {
        $params = $request->all();

        $user = auth()->user();

        $product = $this->product->with('user')->find($params['product_id']);
        $productPrice = $this->productPrice->find($params['product_price_id']);

        $transactionPrice = $params['quantity'] * $productPrice['product_price'];

        $delivery = $this->delivery->create(['address_id' => $params['address_id']]);

        // $delivery['id'] = 44;

        $produceLeaseTime = '';
        switch($productPrice['product_price_type']){
            case "daily":
                $produceLeaseTime = "day";
                break;
            case "weekly":
                $produceLeaseTime = "week";
                break;
            case "monthly":
                $produceLeaseTime = "month";
                break;
            case "yearly":
                $produceLeaseTime = "year";
                break;
        }
        $leaseEndDate = date('Y-m-d', strtotime('+' . $params['lease_time_recurrence'] . ' ' . $produceLeaseTime, strtotime($params['start_date'])));

        // $leaseEndDate = date('Y-m-d', strtotime('+7 days', strtotime($params['start_date'])));

        $data = [
            'consumer_id' => $user['id'],
            'provider_id' => $product['user']['id'],
            'transaction_price' => $transactionPrice,
            'transaction_delivery' => $delivery['id'],
            'transaction_status' => 'unpaid',
            'lease_start_date' => $params['start_date'],
            'lease_end_date' => $leaseEndDate
        ];

        $leaseTransaction = $this->leaseTransaction->create($data);

        $this->leaseTransactionDetail->create([
            'lease_transaction_id' => $leaseTransaction['id'],
            'product_id' => $product['id'],
            'product_quantity' => $params['quantity'],
            'product_price' => $productPrice['product_price']
        ]);

        return Response::data($leaseTransaction);
    }
    // This function pays the following item mentioned. Shows the amount, currency, description, recourse and receipt email of how much they pay. 
    public function pay(Request $request) {
        $request->validate([
            'transaction_id' => 'required'
        ]);
        $params = $request->toArray();

        $transaction = $this->leaseTransaction->find($params['transaction_id']);

        $transaction['transaction_price'] = 31805300;

        Stripe::setApiKey('sk_test_h7y8zciBAuwuNqaR5EMLkifD00KBvZJpyI');
        Charge::create([
            'amount' => $transaction['transaction_price'],
            'currency' => $params['checkout']['token']['card']['currency'],
            'description' => 'Payment for order with id ' . $params['transaction_id'],
            'source' => $params['checkout']['token']['id'],
            'receipt_email' => $transaction['consumer']['email']
        ]);

        $transaction->fill([
            'transaction_status' => 'paid(1)',
        ]);
        $transaction->save();

        return Response::message('Payment success. You can check it in your transaction history. Thank you!');
    }
    // This function checks if the data is found or not found.
    public function showLease($id) {
        $transaction = $this->leaseTransaction->with(['transactionDetails.product.productImages', 'transactionDelivery.address'])->find($id);

        if (!$transaction) {
            return Response::error([
                'error' => 'data_not_found',
                'message' => 'Transaction with transaction_id \'' . $id . '\' not found'
            ]);
        }

        return Response::data($transaction);
    }
    //This function shows all the transaction that the product pays or ongoing. 
    public function showAllTransaction(){
        $user = auth()->user();
        $transactions = $this->leaseTransaction->with(['transactionDetails.product','provider','transactionDelivery'])->where('consumer_id',"=",$user['id'])->get();
        return Response::data($transactions);
    }
    //This function shows all the consumer transaction time.
    public function showAllConsumerTrans(){
        $user = auth()->user();
        $transactions = $this->leaseTransaction->with(['transactionDetails.product','deliveries','transactionDelivery'])->where('provider_id',"=",$user['id'])->get();
        return Response::data($transactions);
    }

    // This function shows the items based on status whether it was payed or ongoing.
    public function showAllBasedOnStatus(LeaseTransactionStatusRequest $request){
        $params = $request->toArray();
        $user = auth()->user();
        $transactions = $this->leaseTransaction->with(['transactionDetails.product.productImages','transactionDelivery.address'])
                                                ->where('provider_id',"=",$user['id'])
                                                ->where('transaction_status','=',$params['status'])
                                                ->get();
        return Response::data($transactions);
    }


    //This function requires to input the transaction id, erceepit no and the status when the product was payed. 
    public function leaseDelivery(Request $request) {
        $request->validate([
            'transaction_id' => 'required',
            'receipt_no' => 'required'
        ]);

        $transaction = $this->leaseTransaction->find($request->get('transaction_id'));
        $transaction->fill([
            'transaction_status' => 'delivery',
        ]);
        $transaction->save();

        $delivery = $this->delivery->find($transaction['transaction_delivery']);
        $delivery->fill([
            'delivery_date' => date('Y-m-d'),
            'delivery_reciept' => $request->get('receipt_no')
        ]);
        $delivery->save();

        return Response::message('Input delivery data done.');

    }
}
