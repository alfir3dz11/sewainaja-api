<?php

namespace App\Http\Controllers;

use App\Http\Requests\Register\RegisterRequest;
use App\Services\Response;
use App\User;

class RegisterController extends Controller
{
    private $user;

    public function __construct(User $user) {
        $this->user = $user;
    }

    public function register(RegisterRequest $request) {
        $params = $request->except('confirm_password');

        $params['role'] = 'customer';
        $params['password'] = bcrypt($params['password']);

        $user = $this->user->create($params);

        $token = auth()->login($user);

        return Response::data([
            'token' => $token
        ]);
    }
}
