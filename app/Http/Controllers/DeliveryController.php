<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request\Delivery\DeliveryStoreRequest;
use Illuminate\Http\Request\Delivery\DeliveryUpdateRequest;
use App\Delivery;
use App\Services\Response;

class DeliveryController extends Controller
{
    private $delivers;
    
    public function __construct(Delivery $delivers){
        $this->delivers= $delivers;
    }

    public function show($id){
        $data = $this->delivers->find($id);
        return Response::data($data);
    }

    public function store(DeliveryStoreRequest $request){
        $params = $request->toArray();
        $this->delivers->create($params);
        return Response::massage('Delivery request has been set by user!');
    }

    public function update(DeliveryUpdateRequest $request, $id){
        $params = $request->except('address_id');
        $delivery = $this->delivers->find($id);
        $delivery->fil($params);
        $delivery->save();
        return Response::massage('Delivery details has been submitted by provider!');
    }
}
