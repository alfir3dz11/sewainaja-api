<?php

namespace App\Http\Controllers;

use App\Http\Requests\LeaseReturn\LeaseReturnStoreRequest;
use App\Http\Requests\LeaseReturn\LeaseReturnUpdateRequest;
use App\LeaseTransaction;
use App\LeaseTransactionDetail;
use App\Product;
use App\LeaseReturn;
use App\Address;
use App\Delivery;
use App\Services\Response;
use Illuminate\Http\Request;


class LeaseReturnController extends Controller
{
    private $LeaseReturn;
    private $leaseTransaction;
    private $leaseTransactionDetail;
    private $product;
    private $delivery;
    private $address;

    public function __construct(
        LeaseTransaction $leaseTransaction,
        LeaseTransactionDetail $leaseTransactionDetail,
        LeaseReturn $leaseReturn,
        Product $product,
        Address $address,
        Delivery $delivery
    ){
        $this->leaseReturn = $leaseReturn;
        $this->leaseTransaction = $leaseTransaction;
        $this->leaseTransactionDetail = $leaseTransactionDetail;
        $this->product = $product;
        $this->delivery = $delivery;
        $this->address = $address;
    }

    public function sendReturn(LeaseReturnStoreRequest $request){
        $params = $request->all();
        $user = auth()->user();
        $leaseTrans = $this->leaseTransaction->where('consumer_id', '=', $user['id'])->where('id','=',$params['lease_transaction_id'])->first();
        $provAddress = $this->address->where('user_id','=',$leaseTrans['provider_id'])->first();
        $delivery = $this->delivery->create([
            'address_id'=> $provAddress['id'], 
            'delivery_date'=> $params['delivery_date'],
            'delivery_reciept' => $params['delivery_reciept']
        ]);
        $leaseReturn = $this->leaseReturn->create([
            'lease_transaction_id' => $params['lease_transaction_id'],
            'return_delivery_id' => $delivery['id'],
            'lease_return_status' => 'send'
        ]);
        return Response::data($leaseReturn);
    }

    public function arrive(LeaseReturnUpdateRequest $request){
        $params = $request->all();
        $leaseReturn = $this->leaseReturn->where('lease_transaction_id','=',$params['lease_transaction_id'])->first();
        $leaseReturn->fill([
            'lease_return_status' => 'recieved'
        ]);
        $leaseReturn->save();
        return Response::message('Transaction Finished');
    }

    public function showProviderReturn(){
        $user = auth()->user();
        $returnAll = $this->leaseReturn->with([
            'leaseTransaction' => function($query) use($user){
                $query->where('lease_transactions.provider_id',"=",$user['id']);
            }
        ])->get();
        return Response::data($returnAll);
    }

    public function showUserReturn(){
        $user = auth()->user();
        $returnAll = $this->leaseReturn->with([
            'leaseTransaction' => function($query) use($user){
                $query->where('lease_transactions.consumer_id',"=",$user['id']);
            }
        ])->get();
        return Response::data($returnAll);
    }

}
