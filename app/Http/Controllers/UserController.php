<?php

namespace App\Http\Controllers;

use App\User;
use App\Services\Response;
use Illuminate\Http\Request;

class UserController extends Controller
{
	private $users;

	public function __construct(LeaseTransaction $users){
        $this->users = $users;
    }


    public function updateRole(Request $request) {
        $data = $this->users->find($request['user_id']);

        $data->fill([
        	'role' => 'provider'

        ]);
        $data->save();
        return Response::data($data);
    }
}