<?php

namespace App\Services;

use Illuminate\Contracts\Support\MessageBag;

class Response
{
    public static function data($data = []) {
        return response()->json([
            'data' => $data,
            'meta' => [
                'version' => 1
            ]
        ], 200);
    }

    public static function message($message) {
        return response()->json([
            'message' => $message,
            'meta' => [
                'version' => 1
            ]
        ], 200);
    }

    public static function error($error, $status = 404) {
        return response()->json([
            'errors' => [
                'error' => $error['error'],
                'message' => $error['message']
            ],
            'meta' => [
                'version' => 1
            ]
        ], $status);
    }

    public static function validationError(MessageBag $errors) {
        $errorMessages = [];

        foreach ($errors->all() as $error) {
            $errorMessages[] = $error;
        }

        return response()->json([
            'errors' => $errorMessages,
            'meta' => [
                'version' => 1
            ]
        ], 422);
    }
}
