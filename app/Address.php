<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    protected $table = 'address';
    protected $fillable = ['user_id','province', 'city', 'address', 'zipcode'];

    public function user(){
        return $this->belongsTo('App\User','user_id');
    }
}
