<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LeaseTransaction extends Model
{
    protected $table = 'lease_transactions';
    protected $fillable = ['consumer_id','provider_id','transaction_price','transaction_delivery','transaction_status','payment_date_time','lease_start_date','lease_end_date', 'user_billing_id'];
    protected $with = ['consumer'];
    
    public function consumer(){
        return $this->belongsTo('App\User','consumer_id');
    }

    public function provider(){
        return $this->belongsTo('App\User','provider_id');
    }

    public function transactionDelivery(){
        return $this->belongsTo('App\Delivery','transaction_delivery');
    }

    public function transactionDetails() {
        return $this->hasMany('App\LeaseTransactionDetail', 'lease_transaction_id');
    }

}
