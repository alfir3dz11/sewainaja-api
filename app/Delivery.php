<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Delivery extends Model
{
    protected $table = 'deliveries';
    protected $fillable = ['delivery_date', 'address_id','delivery_reciept'];

    public function address(){
        return $this->belongsTo('App\Address','address_id');
    }
}
