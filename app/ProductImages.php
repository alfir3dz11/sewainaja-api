<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductImages extends Model
{
    protected $table = 'product_images';
    protected $fillable = ['product_id','image_link','image_descriptions'];
    protected $appends = ['image'];

    public function getImageAttribute() {
        return asset($this->image_link);
    }
}
